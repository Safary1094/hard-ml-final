# Deploy QA Service
Сафаралеев Алексей  +7 921 879 94 45

Деплой и обновление QA-сервиса выполняется с помощью gitlab-pipelines, каждый из которых запускает ansible-playbook

## Основные компоненты
1. nginx
2. gateway
3. embedder
4. swarm-cluster:
    * index_0 (replica 1)
    * index_0 (replica 2)
    * index_1 (replica 1)
    * index_1 (replica 2)
    * index_2 (replica 1)
    * index_2 (replica 2)
    * index_3 (replica 1)
    * index_3 (replica 2)
    
## Вспомогательные компоненты
1. gitlab-runner
2. файловый сервер (в идеале s3, но для простоты используем `/var/data` на одной из машин)
3. docker-registry
    
![](imgs/general_scema.png)

## Конфигурирование сервиса
В файле [vars/hosts](vars/hosts) необходимо задать адреса машин для запуска соответствующих ansible-playbook

В файле [vars/vars.yml](vars/vars.yml) задаем переменные для ansible:

* порты embedder, gateway и временного gateway (для обновления)
* параметры `indices` - конфигурация каждого индекса: ip и порт, номер кластера, локальный путь до индекса
* параметры `v2indices` - конфигурация индекса следующей версии

NB! `indices` и `v2indices` не привязаны жестко к 1 и 2 версии данных .  `indices`- это всегда текущая версия индекса, `v2indices` - следующая версия. Эти параметры автоматически переписываются всякий раз после успешного обновления

## Физические компоненты
4 виртуальные машины, предоставляемые `KarpovCourses`, при этом одна полностью выделена под gitlab-runner, а остальные три участвуют непосредственно в сервинге. Согласно конфигурации, сервис будет развернут следующим образом:

![схема хостов](imgs/physical_schema.png)


## Deploy
Все шаги необходимые для деплоя запускаются через gitlab-job, описанные в [ci/.gitlab-ci.yml](ci/.gitlab-ci.yml)

1. запустим `vm-preparion-job`, которая запустит [playbooks/vm.yml](playbooks/vm.yml), который на все машины устанавливает python, docker, конфигурирует docker-daemon, производит обмен ключами между нодами
2. запустим `preparatioт-job`, которая запустит [playbooks/preparation.yml](playbooks/preparation.yml) Внутри контейнера [roles/preparation/files/Dockerfile](roles/preparation/files/Dockerfile) запустится разделение эмбеддингов и маппингов, а результатом джобы станет доставка этих файлов адресно на каждую ноду
3. запустим `deploy-job`, которая запустит [playbooks/deploy.yml](playbooks/deploy.yml), которая:
    * поднимает Deploy docker registry
    * билдит и пушит контейнеры (для [индекса](roles/docker_image/files/index/), [эмбеддера](roles/docker_image/files/embedder) и [gateway](roles/docker_image/files/gateway/))
    * разворачивает эмбеддер
    * разворачивает gateway
    * разворачивает docker-swarm
    * разворачивает сервисы-индексы, согласно заданным hostname, т.е. каждый индекс прибит к определенной ноде
    * разворачивает nginx

## Update

Для обновления сервиса необходимо запустить CI-джобу `update-job`, которая:
1. запустит [playbooks/green-blue-deploy.yml](playbooks/green-blue-deploy.yml)
2. обновит файл [vars/vars.yml](vars/vars.yml) в котором указаны порты и локальные пути к данным для текущих индексов и для следующего обновления (generation3)

В случае неудачного обновления индексов, происходит откат к текущей версии