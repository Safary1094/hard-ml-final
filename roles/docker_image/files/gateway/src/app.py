import pickle
import re
import flask
import numpy as np
import requests
from flask import Flask
from flask import request
import os
import ast

app = Flask(__name__)

# index_routes = {
#     '0': '{{ groups['proxy'][0] }}:{{ indices['index0']['publish_port'] }}',
#     '1': '{{ groups['proxy'][0] }}:{{ indices['index1']['publish_port'] }}',
#     '2': '{{ groups['proxy'][0] }}:{{ indices['index2']['publish_port'] }}',
#     '3': '{{ groups['proxy'][0] }}:{{ indices['index3']['publish_port'] }}',
# }

# embedding_server_route = 'http://{{ groups['embedder'][0] }}:{{ embedder_port }}/embed_text'
embedding_server_route = 'http://172.23.195.133:8888/embed_text'

# data_path = os.environ.get('DATA_PATH', '/var/data/generation1')
# gen = 1 if data_path == '/var/data/generation1' else 2

# with open(f'{data_path}/clusters_centers_use_dg{gen}.pkl', 'rb') as f:
#     centers = pickle.load(f)

def get_embedding(text) -> np.ndarray:
    embedder_result = requests.get(embedding_server_route, json={'text': text})
    emb = embedder_result.json()
    print(emb)
    emb = emb['embed'].replace('\n', '')
    s2 = re.sub('(\d) +(-|\d)', r'\1,\2', emb)
    res = ast.literal_eval(s2)
    return np.array(res)


def find_nearest_cluster(emb: np.ndarray) -> int:
    dists = []
    for cluster_i, center in centers.items():
        d = (emb - center) ** 2
        d = d.sum()
        dists.append(d**0.5)
    dists = np.stack(dists)
    return dists.argmin(axis=0)


def get_k_nearest_from_index(cluster_i, emb: np.ndarray):
    index_route = index_routes[cluster_i]
    k_nearest = requests.post(index_route, json={'embedding': np.array2string(emb)})
    k_nearest = k_nearest.json()
    return k_nearest['k_nearest']


@app.route("/find_silmilar_questions", methods=['GET', 'POST'])
def find_silmilar_questions():
    text = request.get_json()['text']

    emb = get_embedding(text)
    print(emb)
    cluster_ind = find_nearest_cluster(emb)
    k_nearest = get_k_nearest_from_index(cluster_ind, emb)

    return flask.jsonify({'k_nearest': k_nearest})

@app.route("/")
def check_health():
    return 'Ok'

if __name__ == '__main__':
    app.run(host='0.0.0.0')
