import requests
import sys
import re

with open('data.example') as data:
    data_example = data.read()

    addr = 'http://127.0.0.1:5000/'

    res = requests.get(addr, json={'embedding': data_example})
    ret = 0 if res.status_code == 200 else 1
    sys.exit(ret)
