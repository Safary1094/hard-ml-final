from flask import Flask
import faiss
import pickle
from flask import request
import flask
import numpy as np
import ast

app = Flask(__name__)

emb_path = "/var/data/use_embeddings_np.pkl"
map_path = "/var/data/mapping.pkl"

index_dim = 512
index = faiss.IndexFlatL2(index_dim)

with open(emb_path, 'rb') as handle:
    embs = pickle.load(handle)
with open(map_path, 'rb') as handle:
    mapping = pickle.load(handle)

index.add(embs)

@app.route("/test")
def test():
    return "It's a test page"

@app.route("/")
def main_page():
    input_vector = request.json['embedding']
    input_vector = np.array(ast.literal_eval(input_vector))
    nearest_results_num = 30
    D, I = index.search(input_vector, nearest_results_num)

    return flask.jsonify({'k_nearest': np.array2string(I) })

if __name__ == '__main__':
    app.run(host='0.0.0.0')
