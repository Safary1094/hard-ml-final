# !pip3 install tensorflow_text>=2.0.0rc0
import tensorflow_text
import tensorflow_hub as hub

import flask
import numpy as np
from flask import Flask
from flask import request
from tensorflow.python.ops.numpy_ops import np_config

np_config.enable_numpy_behavior()

app = Flask(__name__)

embed = hub.load("emb_tf_model/")

@app.route("/embed_text", methods=['GET', 'POST'])
def embed_text():
    text = request.get_json()
    embed_result = embed(text['text'])
    return flask.jsonify({'embed': np.array2string(embed_result)})

@app.route("/")
def check_health():
    return 'Ok'

if __name__ == '__main__':
    app.run(host='0.0.0.0')
