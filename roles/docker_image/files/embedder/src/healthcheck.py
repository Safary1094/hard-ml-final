import requests
import sys

addr = 'http://127.0.0.1:5000/embed_text'

res = requests.get(addr, json={'text': 'to be or not to be?'})

ret = 0 if res.status_code == 200 else 1
sys.exit(ret)
