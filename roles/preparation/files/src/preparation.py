import pickle
import numpy as np
import json
import os

def split_clusters():
    data_path = os.environ.get('DATA_PATH', '/var/data/generation1')
    gen = 1 if data_path == '/var/data/generation1' else 2

    # read clusters info
    with open(f"{data_path}/clusters_use_dg{gen}.json") as f:
        clusters = json.load(f)

    with open(f"{data_path}/use_embeddings_dg{gen}.pkl", 'rb') as f:
        embeddings = pickle.load(f)

    for cluster_id, questions in clusters.items():
        emb = [embeddings[q] for q in questions]
        mapping = questions
        emb_np = np.concatenate(emb)

        with open(f'/var/temp_data/use_embeddings_np_cluster{cluster_id}.pkl', 'wb') as handle:
            pickle.dump(emb_np, handle, protocol=pickle.HIGHEST_PROTOCOL)

        with open(f'/var/temp_data/mapping_cluster{cluster_id}.pkl', 'wb') as handle:
            pickle.dump(mapping, handle, protocol=pickle.HIGHEST_PROTOCOL)


split_clusters()
