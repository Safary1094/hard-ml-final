import argparse
from pathlib import Path
import yaml

def generate(path: Path):
    with open(path, "r") as stream:
        vars = yaml.safe_load(stream)
        temp_gateway_port = vars['temp_gateway_port']
        gateway_port = vars['gateway_port']
        new_indices = vars['v2indices']
        current_indices = vars['indices']

        vars['gateway_port'] = temp_gateway_port
        vars['temp_gateway_port'] = gateway_port
        vars['indices'] = new_indices
        vars['v2indices'] = current_indices

    with open(path, "w") as stream:
        yaml.dump(vars, stream)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", help="Path to file with variables", required=True, type=Path, dest='path')
    known_args = parser.parse_args()
    generate(known_args.path)

